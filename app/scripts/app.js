﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Footer
    * @version 1.0.0
    * @Componente Footer para rodapé de páginas
    */

    angular.module('funcef-footer.directive', []);

    angular
    .module('funcef-footer', [
      'funcef-footer.directive'
    ]);
})();