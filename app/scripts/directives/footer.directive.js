﻿(function () {
    'use strict';

    angular
      .module('funcef-footer.directive')
      .directive('ngfFooter', ngfFooter);

    /* @ngInject */
    function ngfFooter() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/footer.view.html',
            scope: {
                version: '@',
                ano: '@',
                open: '=',
                withAside: '='
            }
        };
    }
})();


