# Componente - FUNCEF-Footer

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Footer mostra a informação de rodapé.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-footer --save.
```

## Script

```html
<script src="bower_components/funcef-footer/dist/funcef-footer.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-footer/dist/funcef-footer.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-footer dentro do módulo do Sistema.

```js
angular
    .module('funcef-demo', ['funcef-footer']);
```

## Uso

```html
<footer class="footer" ng-class="{ 'open': open, 'with-aside' : withAside}">
    &copy; {{ ano }} - FUNCEF
    <br /> Versão: {{ version }}
</footer>
```

## Desinstalação:

```
bower uninstall funcef-footer --save
```
