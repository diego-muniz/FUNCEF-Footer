(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */

    angular.module('funcef-footer.directive', []);

    angular
    .module('funcef-footer', [
      'funcef-footer.directive'
    ]);
})();;;(function () {
    'use strict';

    angular
      .module('funcef-footer.directive')
      .directive('ngfFooter', ngfFooter);

    /* @ngInject */
    function ngfFooter() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/footer.view.html',
            scope: {
                version: '@',
                ano: '@',
                open: '=',
                withAside: '='
            }
        };
    }
})();


;angular.module('funcef-footer').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/footer.view.html',
    "<footer class=\"footer\" ng-class=\"{ 'open': open, 'with-aside' : withAside}\"> &copy; {{ ano }} - FUNCEF <br> Versão: {{ version }} </footer>"
  );

}]);
